export const LIST = ["silent", "trace", "debug", "verbose", "info", "warn", "error", "fatal"];
export const MAPPED = {
  silent: 0,
  trace: 1,
  debug: 2,
  verbose: 3,
  info: 4,
  warn: 5,
  error: 6,
  fatal: 7,
};
export const DEFAULT_NAME = "info";
export const DEFAULT_VALUE = MAPPED[DEFAULT_NAME];

export {nameToValue, valueToName, valueToFloorName, getLevelValue};

/*==================================================== Functions  ====================================================*/

function nameToValue(name) {
  if (MAPPED.hasOwnProperty(name)) { return MAPPED[name]; }
  throw new Error("Level string '" + name + "' could not be translated into a number.");
}

function getLevelValue(nameOrValue, fallback) {
  if (typeof nameOrValue === "string") {
    if (MAPPED.hasOwnProperty(nameOrValue)) { return MAPPED[nameOrValue]; }
    const numValue = +nameOrValue;
    if (!Number.isNaN(numValue)) { return numValue; }
  }
  return typeof nameOrValue === "number" ? nameOrValue : fallback;
}

function valueToName(level) { return LIST.hasOwnProperty(level) ? LIST[level] : level + ""; }

function valueToFloorName(level) {
  if (!Number.isInteger(level)) { level = level | 0; }
  if (level < 0) { return LIST[0]; }
  if (level >= LIST.length) { return LIST[LIST.length - 1]; }
  return LIST[level];
}
